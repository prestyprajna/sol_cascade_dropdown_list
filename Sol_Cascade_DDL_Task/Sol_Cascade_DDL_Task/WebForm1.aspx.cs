﻿using Newtonsoft.Json;
using Sol_Cascade_DDL_Task.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Cascade_DDL_Task
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        #region  declaration
        private UserDal _dalObj = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack==false)
            {
                _dalObj = new UserDal();

                this.CreateUserDalInstance = _dalObj;

                this.BindCountryData();
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindStateData(Convert.ToDecimal(ddlCountry.SelectedValue));
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindCityData(Convert.ToDecimal(ddlState.SelectedValue));
        }

        #region  private methods

        private void BindCountryData()
        {
            var countryDataObj =this.CreateUserDalInstance                
                .GetCountryData(null);

            //clear all items of dropdown list
            ddlCountry.DataSource = null;
            ddlCountry.Items.Clear();

            //bind country data from database to dropdowm list
            ddlCountry.DataSource = countryDataObj;
            ddlCountry.DataBind();

            //set initial value in dropdown list
            ddlCountry.AppendDataBoundItems = true;
            ddlCountry.Items.Insert(0, new ListItem("--select country--", "0"));
            //ddlCountry.SelectedIndex = 0;
        }

        private void BindStateData(decimal? id)
        {
            var stateDataObj = this.CreateUserDalInstance?.GetStateData(new Models.StateEntity()
            {
                CountryId=id
            });

            //clear all items of dropdown list
            ddlCountry.DataSource = null;
            ddlCountry.Items.Clear();

            //bind country data from database to dropdowm list
            ddlCountry.DataSource = stateDataObj;
            ddlCountry.DataBind();

            //set initial value in dropdown list
            ddlCountry.AppendDataBoundItems = true;
            ddlCountry.Items.Insert(0, new ListItem("--select state--", "0"));
            //ddlCountry.SelectedIndex = 0;
        }

        private void BindCityData(decimal? id)
        {
            var cityDataObj = this.CreateUserDalInstance?.GetCityData(new Models.CityEntity() {
                StateId=id
            });

            //clear all items of dropdown list
            ddlCountry.DataSource = null;
            ddlCountry.Items.Clear();

            //bind country data from database to dropdowm list
            ddlCountry.DataSource = cityDataObj;
            ddlCountry.DataBind();

            //set initial value in dropdown list
            ddlCountry.AppendDataBoundItems = true;
            ddlCountry.Items.Insert(0, new ListItem("--select city--", "0"));
            //ddlCountry.SelectedIndex = 0;
        }

        #endregion

        #region  private property

        private UserDal CreateUserDalInstance
        {
            get
            {
                return
                    _dalObj
                    ??
                    (JsonConvert.DeserializeObject<UserDal>(ViewState["UserDalObject"] as string));
                    
            }

            set
            {
                _dalObj = value;
                ViewState["UserDalObject"] = JsonConvert.SerializeObject(_dalObj);
            }
        }
        #endregion
    }
}