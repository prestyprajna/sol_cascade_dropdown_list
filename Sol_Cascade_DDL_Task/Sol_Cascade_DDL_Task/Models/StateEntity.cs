﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Cascade_DDL_Task.Models
{
    public class StateEntity
    {
        public Decimal? StateId { get; set; }

        public string StateName { get; set; }

        public Decimal? CountryId { get; set; }
    }
}