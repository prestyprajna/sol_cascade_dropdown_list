﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Cascade_DDL_Task.Models
{
    public class CountryEntity
    {
        public Decimal? CountryId { get; set; }

        public string CountryName { get; set; }
    }
}