﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Cascade_DDL_Task.Models
{
    public class CityEntity
    {
        public Decimal? CityId { get; set; }

        public string CityName { get; set; }

        public Decimal? StateId { get; set; }
    }
}