﻿using Sol_Cascade_DDL_Task.DAL.ORD;
using Sol_Cascade_DDL_Task.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Cascade_DDL_Task.DAL
{
    public class UserDal
    {
        #region  declaration
        private CountryDCDataContext _dc = null;
        #endregion

        #region   constructor
        public UserDal()
        {
            _dc = new CountryDCDataContext();
        }
        #endregion


        #region   public methods

        public List<CountryEntity> GetCountryData(CountryEntity countryEntityObj)
        {
            int? status = null;
            string message = null;

            var getQuery=_dc?.uspGetCountry(
                "GetCountryData",
                countryEntityObj?.CountryId,
                countryEntityObj?.CountryName,
                ref status,
                ref message
                )
                ?.AsEnumerable()
                ?.Select((leCountryObj) => new CountryEntity()
                {
                    CountryId = leCountryObj?.CountryId,
                    CountryName = leCountryObj?.CountryName
                })
                ?.ToList();

            return getQuery;
        }

        public List<StateEntity> GetStateData(StateEntity stateEntityObj)
        {
            int? status = null;
            string message = null;

            var getQuery = _dc?.uspGetState
                (
                "GetStateData",
                stateEntityObj?.StateId,
                stateEntityObj?.StateName,
                stateEntityObj?.CountryId,
                ref status,
                ref message
                )
                ?.AsEnumerable()
                ?.Select((leStateObj) => new StateEntity()
                {
                    StateId = leStateObj?.StateId,
                    StateName = leStateObj?.StateName
                    //CountryId = leStateObj?.CountryId
                })
                ?.ToList();
            return getQuery;
        }

        public List<CityEntity> GetCityData(CityEntity cityEntityObj)
        {
            int? status = null;
            string message = null;

            var getQuery = _dc?.uspGetCity
                (
                "GetCityData",
                cityEntityObj?.CityId,
                cityEntityObj?.CityName,
                cityEntityObj?.StateId,
                ref status,
                ref message
                )
                ?.AsEnumerable()
                ?.Select((leCityObj) => new CityEntity()
                {
                    CityId = leCityObj?.CityId,
                    CityName = leCityObj?.CityName
                    //CountryId = leStateObj?.StateId
                })
                ?.ToList();

            return getQuery;
        }

        #endregion




    }
}